import 'package:majootestcase/bloc/obsecure_text_bloc/obsecure_text_bloc_cubit.dart';
import 'package:majootestcase/repository/api_repository.dart';
import 'package:majootestcase/services/api_service.dart';
import 'package:majootestcase/ui/splash_screen/splash_screen_page.dart';
import 'bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/home_bloc/home_bloc_cubit.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp(
    apiServices: ApiServices(),
  ));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  final ApiServices apiServices;

  const MyApp({Key? key, required this.apiServices}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<ObsecureTextBlocCubit>(
            create: (context) => ObsecureTextBlocCubit()),
        BlocProvider<AuthBlocCubit>(create: (context) => AuthBlocCubit()),
        BlocProvider<HomeBlocCubit>(
            create: (context) => HomeBlocCubit(
                  apiRepository: ApiRepository(apiServices),
                )..fetchMovieTrending()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: SplashScreenPage(),
      ),
    );
  }
}
