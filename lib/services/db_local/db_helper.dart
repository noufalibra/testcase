import 'package:majootestcase/models/user/user_model.dart';
import 'package:sqflite/sqflite.dart' as sql;
import 'dart:async';
import 'dart:io';
import 'package:path_provider/path_provider.dart';

class DbHelper {
  static const String DB_NAME = 'testcase.db';
  static const String TABLE_USER = 'user';
  static const String USERNAME = 'username';
  static const String EMAIL = 'email';
  static const String PASSWORD = 'password';

  static Future<void> createTables(sql.Database database) async {
    await database.execute("""CREATE TABLE $TABLE_USER(
        id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        $USERNAME TEXT,
        $EMAIL TEXT,
        $PASSWORD TEXT,
        createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
      )
      """);
  }

// id: the id of a item
// title, description: name and description of your activity
// created_at: the time that the item was created. It will be automatically handled by SQLite

  static Future<sql.Database> db() async {
    return sql.openDatabase(
      DB_NAME,
      version: 1,
      onCreate: (sql.Database database, int version) async {
        await createTables(database);
      },
    );
  }

  // REGISTER
  static Future<int> register(
      {required String username,
      required String email,
      required String password}) async {
    final db = await DbHelper.db();

    final data = {USERNAME: username, EMAIL: email, PASSWORD: password};
    final id = await db.insert(TABLE_USER, data,
        conflictAlgorithm: sql.ConflictAlgorithm.replace);
    return id;
  }

  // LOGIN
  static Future<UserModel?> login(
      {required String email, required String password}) async {
    final db = await DbHelper.db();
    final res = await db.rawQuery("SELECT * FROM $TABLE_USER WHERE "
        "$EMAIL = '$email' AND "
        "$PASSWORD = '$password'");

    if (res.length > 0) {
      return UserModel.fromMap(res.first);
    }
    print(res);
    return null;
  }

// // Read all items (journals)
// static Future<List<Map<String, dynamic>>> getItems() async {
//   final db = await SQLHelper.db();
//   return db.query('items', orderBy: "id");
// }
//
// // Read a single item by id
// // The app doesn't use this method but I put here in case you want to see it
// static Future<List<Map<String, dynamic>>> getItem(int id) async {
//   final db = await SQLHelper.db();
//   return db.query('items', where: "id = ?", whereArgs: [id], limit: 1);
// }
//
// // Update an item by id
// static Future<int> updateItem(
//     int id, String title, String? descrption) async {
//   final db = await SQLHelper.db();
//
//   final data = {
//     'title': title,
//     'description': descrption,
//     'createdAt': DateTime.now().toString()
//   };
//
//   final result =
//       await db.update('items', data, where: "id = ?", whereArgs: [id]);
//   return result;
// }
//
// // Delete
// static Future<void> deleteItem(int id) async {
//   final db = await SQLHelper.db();
//   try {
//     await db.delete("items", where: "id = ?", whereArgs: [id]);
//   } catch (err) {
//     debugPrint("Something went wrong when deleting an item: $err");
//   }
// }
}
