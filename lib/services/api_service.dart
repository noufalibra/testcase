import 'package:dio/dio.dart';

import 'package:majootestcase/services/dio_config_service.dart' as dioConfig;
import 'package:majootestcase/utils/constant.dart';

class ApiServices {
  Future<Response?> getMovieData() async {
    try {
      await dioConfig.createInstance();
      final response = await dioConfig.dioInstance!.get(BaseUrl.MOVIE_TRENDING,
          queryParameters: {'api_key': BaseUrl.API_KEY});
      return response;
    } catch (err) {
      print('Error: $err');
    }
  }
}
