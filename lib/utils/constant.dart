class Preference {
  static const USER_VALUE = "user_value";
  static const IS_LOGGED_IN = "is_logged_in";
}

class BaseUrl {
  static const API_KEY = "87262c30c6b2462331b949a6244e371a";
  static const BASE_URL = "https://api.themoviedb.org/";
  static const IMAGE_URL = "https://image.tmdb.org/t/p/w500";

  static const MOVIE_TRENDING = BASE_URL + "3/trending/all/day";
}

class Font {}

class ScreenUtilConstants {
  static const width = 320.0;
  static const height = 640.0;
}
