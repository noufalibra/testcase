import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/obsecure_text_bloc/obsecure_text_bloc_cubit.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/main.dart';
import 'package:majootestcase/ui/home/home_page.dart';

import '../../bloc/auth_bloc/auth_bloc_cubit.dart';
import '../../models/user/user_model.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _usernameController = TextController();
  final _emailController = TextController();
  final _passwordController = TextController();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  bool _isLoading = false;

  _mapToStateBloc(BuildContext context, AuthBlocState state) {
    if (state is RegisterBlocLoadingState) {
      showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('Loading...'),
                  CircularProgressIndicator.adaptive()
                ],
              ),
            );
          });
    }
    if (state is RegisterBlocSuccesState) {
      Future.delayed(
          Duration(seconds: 1),
          () => {
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(builder: (context) => HomePage()),
                    (route) => false)
              });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Register'),
      ),
      body: BlocConsumer<AuthBlocCubit, AuthBlocState>(
        listener: _mapToStateBloc,
        builder: bodyRegister,
      ),
    );
  }

  Widget bodyRegister(context, state) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            _buildForm(formKey, context, _usernameController, _emailController,
                _passwordController),
            const SizedBox(
              height: 20,
            ),
            // CustomButton(
            //   text: 'Register',
            //
            // )
            Container(
              width: double.infinity,
              height: 50,
              child: (!_isLoading)
                  ? ElevatedButton(
                      onPressed: () {
                        handleRegister();
                      },
                      child: const Text('REGISTER'),
                    )
                  : CircularProgressIndicator.adaptive(),
            )
          ],
        ),
      ),
    );
  }

  Form _buildForm(
      GlobalKey<FormState> formKey,
      BuildContext context,
      TextController _usernameController,
      TextController _emailController,
      TextController _passwordController) {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _usernameController,
            hint: 'Username',
            label: 'Username',
          ),
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'Example@123.com',
            label: 'Email',
            validator: (val) {
              final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              if (val != null)
                return pattern.hasMatch(val) ? null : 'email is invalid';
            },
          ),
          BlocBuilder<ObsecureTextBlocCubit, bool>(
              builder: (context, state) => CustomTextFormField(
                    context: context,
                    label: 'Password',
                    hint: 'password',
                    controller: _passwordController,
                    isObscureText: state,
                    suffixIcon: IconButton(
                      icon: Icon(
                        state
                            ? Icons.visibility_off_outlined
                            : Icons.visibility_outlined,
                      ),
                      onPressed: () {
                        context
                            .read<ObsecureTextBlocCubit>()
                            .changeObsecureText();
                      },
                    ),
                  )),
        ],
      ),
    );
  }

  void handleRegister() async {
    final String? _username = _usernameController.value;
    final String? _email = _emailController.value;
    final String? _password = _passwordController.value;
    if (formKey.currentState?.validate() == true &&
        _username != null &&
        _email != null &&
        _password != null) {
      UserModel userModel =
          UserModel(userName: _username, email: _email, password: _password);
      context.read<AuthBlocCubit>().register(userModel);
      // authBlocCubit.register(userModel);
    }
  }
}
