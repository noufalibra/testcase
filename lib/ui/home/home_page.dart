import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/ui/home/detail_movie/detail_movie_page.dart';
import 'package:majootestcase/ui/home/widget/card_movie.dart';
import 'package:majootestcase/utils/constant.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    // TODO: implement initState
    context.read<HomeBlocCubit>().fetchMovieTrending();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Movie Trending'),
          actions: [
            IconButton(
                onPressed: () async {
                  showDialog(
                      barrierDismissible: false,
                      context: context,
                      builder: (context) => AlertDialog(
                            title: const Text('Logout'),
                            content:
                                const Text('Apakah Anda yakin ingin keluar ?'),
                            actions: [
                              TextButton(
                                onPressed: () {
                                  context.read<AuthBlocCubit>().logout(context);
                                },
                                child: Text('Ya'),
                              ),
                              TextButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  child: Text(
                                    'Tidak',
                                    style: TextStyle(color: Colors.red),
                                  )),
                            ],
                          ));
                },
                icon: Icon(Icons.logout))
          ],
        ),
        body: BlocBuilder<HomeBlocCubit, HomeBlocState>(
          builder: (context, state) {
            if (state is HomeBlocLoading) {
              return Center(child: const CircularProgressIndicator.adaptive());
            } else if (state is HomeBlocError) {
              return Text(state.failure.message);
            } else if (state is HomeBlocLoaded) {
              final listMovieTrending = state.listMovieTrending;
              return listMovieTrending!.results!.isEmpty
                  ? Center(child: Text('Data Kosong'))
                  : GridView.builder(
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 3,
                              crossAxisSpacing: 15,
                              mainAxisSpacing: 15,
                              childAspectRatio: 1.1),
                      padding: EdgeInsets.all(15),
                      itemCount: listMovieTrending.results!.length,
                      itemBuilder: (context, index) {
                        var data = listMovieTrending.results![index];
                        return CardMovie(
                          judulFilm: data.title ?? '-',
                          image: BaseUrl.IMAGE_URL + data.posterPath.toString(),
                          heroTag: data.id.toString(),
                          onTap: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => DetailMoviePage(
                                        heroTag: data.id.toString(),
                                        image: BaseUrl.IMAGE_URL +
                                            data.posterPath.toString(),
                                        title: data.title ?? '-',
                                        rating: data.voteAverage.toString(),
                                        popularity: data.popularity.toString(),
                                        date: data.releaseDate.toString(),
                                        overview: data.overview.toString(),
                                      ))),
                        );
                      },
                    );
            }
            return const SizedBox.shrink();
          },
        ));
  }
}
