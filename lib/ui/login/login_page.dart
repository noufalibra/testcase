import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/obsecure_text_bloc/obsecure_text_bloc_cubit.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/models/user/user_model.dart';
import 'package:majootestcase/ui/register/register_page.dart';

import '../../main.dart';
import '../home/home_page.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  final _emailController = TextController();
  final _passwordController = TextController();
  bool isLoading = false;
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
  }

  _mapToStateBloc(BuildContext context, AuthBlocState state) {
    if (state is AuthBlocLoadingState) {
      showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('Loading...'),
                  CircularProgressIndicator.adaptive()
                ],
              ),
            );
          });
    }
    if (state is AuthBlocErrorState) {
      Navigator.pop(context);
      showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              content: Text(
                state.error,
                style: TextStyle(color: Colors.red),
              ),
              actions: [TextButton(onPressed: () => Navigator.pop(context), child: Text('Ok'))],
            );
          });
    }
    if (state is AuthBlocLoggedInState) {
      Future.delayed(
          Duration(seconds: 1),
          () => {
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(builder: (context) => HomePage()),
                    (route) => false)
              });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<AuthBlocCubit, AuthBlocState>(
          listener: _mapToStateBloc,
          child: SingleChildScrollView(
            child: Padding(
              padding:
                  EdgeInsets.only(top: 80, left: 20, bottom: 20, right: 20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Center(
                    child: Hero(
                      tag: 'icon',
                      child: Image.asset(
                        'assets/majoo_icon.png',
                        width: MediaQuery.of(context).size.width * 0.3,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  const Text(
                    'Selamat Datang',
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                      // color: colorBlue,
                    ),
                  ),
                  const Text(
                    'Silahkan login terlebih dahulu',
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  const SizedBox(
                    height: 9,
                  ),
                  _form(),
                  const SizedBox(
                    height: 20,
                  ),
                  // CustomButton(
                  //   text: 'Login',
                  //   onPressed: handleLogin,
                  //   height: 100,
                  // ),
                  Container(
                    width: double.infinity,
                    height: 50,
                    child: (!isLoading)
                        ? ElevatedButton(
                            onPressed: () {
                              handleLogin();
                            },
                            child: const Text('LOGIN'),
                          )
                        : CircularProgressIndicator.adaptive(),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Center(
                    child: RichText(
                      text: TextSpan(
                          text: 'Belum memiliki akun ? ',
                          style: const TextStyle(color: Colors.black),
                          children: <TextSpan>[
                            TextSpan(
                                text: 'Register',
                                style:
                                    const TextStyle(color: Colors.blueAccent),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                RegisterPage()));
                                  })
                          ]),
                    ),
                  )
                ],
              ),
            ),
          )),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'Example@123.com',
            label: 'Email',
            validator: (val) {
              final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              if (val != null)
                return pattern.hasMatch(val) ? null : 'email is invalid';
            },
          ),
          BlocBuilder<ObsecureTextBlocCubit, bool>(
            builder: (context, state) => CustomTextFormField(
              context: context,
              label: 'Password',
              hint: 'password',
              controller: _passwordController,
              isObscureText: state,
              suffixIcon: IconButton(
                icon: Icon(
                  state
                      ? Icons.visibility_off_outlined
                      : Icons.visibility_outlined,
                ),
                onPressed: () {
                  context.read<ObsecureTextBlocCubit>().changeObsecureText();
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  void handleLogin() async {
    final String? _email = _emailController.value;
    final String? _password = _passwordController.value;
    if (formKey.currentState?.validate() == true &&
        _email != null &&
        _password != null) {
      UserModel userModel = UserModel(
        email: _email,
        password: _password,
      );
      context.read<AuthBlocCubit>().loginUser(userModel);
    }
  }
}
