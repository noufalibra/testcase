/// page : 1
/// results : [{"genre_ids":[53,28],"original_language":"en","original_title":"All the Old Knives","poster_path":"/g4tMniKxol1TBJrHlAtiDjjlx4Q.jpg","video":false,"vote_average":6.2,"id":294793,"release_date":"2022-04-08","vote_count":62,"title":"All the Old Knives","adult":false,"backdrop_path":"/xicKILMzPn6XZYCOpWwaxlUzg6S.jpg","overview":"When the CIA discovers one of its agents leaked information that cost more than 100 people their lives, veteran operative Henry Pelham is assigned to root out the mole with his former lover and colleague Celia Harrison.","popularity":137.5,"media_type":"movie"},{"poster_path":"/1g0dhYtq4irTY1GPXvft6k4YLjm.jpg","video":false,"vote_average":8.2,"overview":"Peter Parker is unmasked and no longer able to separate his normal life from the high-stakes of being a super-hero. When he asks for help from Doctor Strange the stakes become even more dangerous, forcing him to discover what it truly means to be Spider-Man.","release_date":"2021-12-15","vote_count":11264,"adult":false,"backdrop_path":"/p4ZeIgm14xiLVGJv1hguBDx9QvJ.jpg","title":"Spider-Man: No Way Home","genre_ids":[28,12,878],"id":634649,"original_language":"en","original_title":"Spider-Man: No Way Home","popularity":7848.875,"media_type":"movie"},{"backdrop_path":"/egoyMDLqCxzjnSrWOz50uLlJWmD.jpg","genre_ids":[28,878,35,10751],"original_language":"en","original_title":"Sonic the Hedgehog 2","poster_path":"/6DrHO1jr3qVrViUO6s6kFiAGM7.jpg","video":false,"vote_average":7.8,"title":"Sonic the Hedgehog 2","vote_count":289,"release_date":"2022-03-30","id":675353,"overview":"After settling in Green Hills, Sonic is eager to prove he has what it takes to be a true hero. His test comes when Dr. Robotnik returns, this time with a new partner, Knuckles, in search for an emerald that has the power to destroy civilizations. Sonic teams up with his own sidekick, Tails, and together they embark on a globe-trotting journey to find the emerald before it falls into the wrong hands.","adult":false,"popularity":8634.369,"media_type":"movie"},{"adult":false,"backdrop_path":"/2n95p9isIi1LYTscTcGytlI4zYd.jpg","genre_ids":[80,18],"id":799876,"original_language":"en","original_title":"The Outfit","overview":"Leonard is an English tailor who used to craft suits on London’s world-famous Savile Row. After a personal tragedy, he’s ended up in Chicago, operating a small tailor shop in a rough part of town where he makes beautiful clothes for the only people around who can afford them: a family of vicious gangsters.","poster_path":"/zbGN4QBrKoT3HO7etj4FvlM3OZb.jpg","release_date":"2022-02-25","title":"The Outfit","video":false,"vote_average":7.3,"vote_count":41,"popularity":73.843,"media_type":"movie"},{"adult":false,"backdrop_path":"/hXTWVJMsI9BkxMLliqL1j0FT55t.jpg","genre_ids":[28],"id":606402,"original_language":"ko","original_title":"야차","overview":"Nicknamed after a human-devouring spirit, the ruthless leader of an overseas black ops team takes up a dangerous mission in a city riddled with spies.","poster_path":"/7MDgiFOPUCeG74nQsMKJuzTJrtc.jpg","release_date":"2022-04-08","title":"Yaksha: Ruthless Operations","video":false,"vote_average":7.1,"vote_count":15,"popularity":102.724,"media_type":"movie"},{"original_language":"en","poster_path":"/x6FsYvt33846IQnDSFxla9j0RX8.jpg","vote_average":8.6,"vote_count":201,"overview":"When Steven Grant, a mild-mannered gift-shop employee, becomes plagued with blackouts and memories of another life, he discovers he has dissociative identity disorder and shares a body with mercenary Marc Spector. As Steven/Marc’s enemies converge upon them, they must navigate their complex identities while thrust into a deadly mystery among the powerful gods of Egypt.","first_air_date":"2022-03-30","id":92749,"name":"Moon Knight","original_name":"Moon Knight","origin_country":["US"],"backdrop_path":"/1uegR4uAxRxiMyX4nQnpzbXhrTw.jpg","genre_ids":[10759,10765,9648,18],"popularity":6957.793,"media_type":"tv"},{"poster_path":"/zMryoZIOqbcJ4aGIJOrcMzWd5Ma.jpg","video":false,"id":739993,"overview":"For teenage misfits Hunter and Kevin, the path to glory is clear: Devote themselves to metal. Win Battle of the Bands. And be worshipped like gods.","release_date":"2022-04-08","vote_count":52,"adult":false,"backdrop_path":"/s7DxaxKvDRVeXnw0PaHMgufYGnS.jpg","vote_average":6.9,"genre_ids":[35,18,10402],"title":"Metal Lords","original_language":"en","original_title":"Metal Lords","popularity":124.603,"media_type":"movie"},{"overview":"When three working class kids enroll in the most exclusive school in Spain, the clash between the wealthy and the poor students leads to tragedy.","name":"Elite","original_name":"Élite","backdrop_path":"/91S4u1naygO4npfPB5loeokLHLr.jpg","genre_ids":[80,9648,18],"id":76669,"vote_count":8006,"poster_path":"/3NTAbAiao4JLzFQw6YxP1YZppM8.jpg","first_air_date":"2018-10-05","origin_country":["ES"],"vote_average":8.1,"original_language":"es","popularity":1925.218,"media_type":"tv"},{"original_language":"en","original_title":"Fantastic Beasts: The Secrets of Dumbledore","poster_path":"/jrgifaYeUtTnaH7NF5Drkgjg2MB.jpg","video":false,"vote_average":7.5,"overview":"In an effort to thwart Grindelwald's plans of raising pure-blood wizards to rule over all non-magical beings, Albus Dumbledore enlists his former student Newt Scamander, who agrees to help, though he's unaware of the dangers that lie ahead. Lines are drawn as love and loyalty are tested, even among the truest friends and family, in an increasingly divided wizarding world.","release_date":"2022-04-07","vote_count":70,"title":"Fantastic Beasts: The Secrets of Dumbledore","adult":false,"backdrop_path":"/zGLHX92Gk96O1DJvLil7ObJTbaL.jpg","id":338953,"genre_ids":[14,12],"popularity":933.414,"media_type":"movie"},{"vote_average":6.6,"overview":"Belgian sleuth Hercule Poirot boards a glamorous river steamer with enough champagne to fill the Nile. But his Egyptian vacation turns into a thrilling search for a murderer when a picture-perfect couple’s idyllic honeymoon is tragically cut short.","release_date":"2022-02-09","adult":false,"backdrop_path":"/lRbDyjI7HEaXxflFQbYpqHRGFBJ.jpg","vote_count":1198,"genre_ids":[80,18,9648,53],"id":505026,"original_language":"en","original_title":"Death on the Nile","poster_path":"/kVr5zIAFSPRQ57Y1zE7KzmhzdMQ.jpg","title":"Death on the Nile","video":false,"popularity":1349.328,"media_type":"movie"},{"adult":false,"backdrop_path":"/pLkx5DfowRZZjQYQgMHW6D7QioO.jpg","id":751237,"genre_ids":[27,9648],"original_language":"en","original_title":"Umma","poster_path":"/moLnqJmZ00i2opS0bzCVcaGC0iI.jpg","video":false,"vote_average":5.7,"vote_count":22,"overview":"Amanda and her daughter live a quiet life on an American farm, but when the remains of her estranged mother arrive from Korea, Amanda becomes haunted by the fear of turning into her own mother.","release_date":"2022-03-18","title":"Umma","popularity":43.665,"media_type":"movie"},{"original_language":"en","original_title":"The In Between","poster_path":"/7RcyjraM1cB1Uxy2W9ZWrab4KCw.jpg","video":false,"vote_average":7.1,"overview":"After surviving a car accident that took the life of her boyfriend, a teenage girl believes he's attempting to reconnect with her from the after world.","release_date":"2022-02-11","id":818750,"adult":false,"backdrop_path":"/i9rEpTqC6aIQOWOc4PDEEAE3hFe.jpg","vote_count":63,"genre_ids":[10749,878,18],"title":"The In Between","popularity":81.495,"media_type":"movie"},{"video":false,"vote_average":6.4,"id":406759,"overview":"A mysterious force knocks the moon from its orbit around Earth and sends it hurtling on a collision course with life as we know it.","release_date":"2022-02-03","adult":false,"backdrop_path":"/x747ZvF0CcYYTTpPRCoUrxA2cYy.jpg","title":"Moonfall","genre_ids":[28,12,878],"original_language":"en","original_title":"Moonfall","poster_path":"/odVv1sqVs0KxBXiA8bhIBlPgalx.jpg","vote_count":603,"popularity":7917.508,"media_type":"movie"},{"first_air_date":"2022-03-24","vote_average":8.8,"original_name":"Halo","origin_country":["US"],"name":"Halo","backdrop_path":"/1qpUk27LVI9UoTS7S0EixUBj5aR.jpg","id":52814,"genre_ids":[10759,10765],"vote_count":361,"original_language":"en","overview":"Depicting an epic 26th-century conflict between humanity and an alien threat known as the Covenant, the series weaves deeply drawn personal stories with action, adventure and a richly imagined vision of the future.","poster_path":"/nJUHX3XL1jMkk8honUZnUmudFb9.jpg","popularity":6988.19,"media_type":"tv"},{"first_air_date":"2022-04-09","name":"SPY x FAMILY","vote_average":9.1,"overview":"Master spy Twilight is the best at what he does when it comes to going undercover on dangerous missions in the name of a better world. But when he receives the ultimate impossible assignment—get married and have a kid—he may finally be in over his head!\n\nNot one to depend on others, Twilight has his work cut out for him procuring both a wife and a child for his mission to infiltrate an elite private school. What he doesn't know is that the wife he's chosen is an assassin and the child he's adopted is a telepath!","vote_count":7,"id":120089,"poster_path":"/3r4LYFuXrg3G8fepysr4xSLWnQL.jpg","original_language":"ja","original_name":"SPY×FAMILY","origin_country":["JP"],"genre_ids":[16,35,10759,80],"backdrop_path":"/LqrXjf2igDXgGl0QakgqPwf4Zy.jpg","popularity":107.22,"media_type":"tv"},{"overview":"In this riveting spy thriller, no one is safe. Harris, a CIA interrogator at an Agency black site, finds himself the target of a rendition operation after being scapegoated for an interrogation gone horribly wrong. As the team tasked to bring Harris in begins to question their orders -- and each other --Olsen (Mel Gibson), a senior intelligence officer, and his subordinate, Visser, raise the stakes. Now, it's up to Harris and some newfound allies to uncover the truth and turn the tables.","release_date":"2022-04-08","adult":false,"backdrop_path":"/rKc7JIj0LG5DjaupAHBa0eOYXdB.jpg","genre_ids":[28,53],"vote_count":3,"id":872542,"original_title":"Agent Game","poster_path":"/qXJFjgcV7ESRHUSxZiBA4PzRMIx.jpg","title":"Agent Game","video":false,"vote_average":7.0,"original_language":"en","popularity":93.502,"media_type":"movie"},{"genre_ids":[9648,10765],"original_language":"en","poster_path":"/lFf6LLrQjYldcZItzOkGmMMigP7.jpg","name":"Severance","vote_average":8.0,"vote_count":59,"id":95396,"overview":"Mark leads a team of office workers whose memories have been surgically divided between their work and personal lives. When a mysterious colleague appears outside of work, it begins a journey to discover the truth about their jobs.","first_air_date":"2022-02-17","original_name":"Severance","origin_country":["US"],"backdrop_path":"/npD65vPa4vvn1ZHpp3o05A5vdKT.jpg","popularity":218.818,"media_type":"tv"},{"adult":false,"backdrop_path":"/ndCSoasjIZAMMDIuMxuGnNWu4DU.jpg","genre_ids":[14,28,12],"original_language":"en","original_title":"Doctor Strange in the Multiverse of Madness","poster_path":"/wRnbWt44nKjsFPrqSmwYki5vZtF.jpg","title":"Doctor Strange in the Multiverse of Madness","video":false,"vote_average":0.0,"overview":"Doctor Strange, with the help of mystical allies both old and new, traverses the mind-bending and dangerous alternate realities of the Multiverse to confront a mysterious new adversary.","id":453395,"vote_count":0,"release_date":"2022-05-04","popularity":420.06,"media_type":"movie"},{"genre_ids":[18],"title":"Dancing on Glass","original_language":"es","original_title":"Las niñas de cristal","poster_path":"/ppF495JNP6poCCEAm4xM1nJEZzD.jpg","video":false,"vote_average":6.5,"id":800407,"overview":"When immense pressure threatens a ballerina in a new lead role, she and another dancer escape into a friendship that isolates them from the real world.","release_date":"2022-04-08","adult":false,"backdrop_path":"/iCltUggpq3F8AAJT4zxJY7OVYFl.jpg","vote_count":4,"popularity":90.654,"media_type":"movie"},{"adult":false,"backdrop_path":"/5P8SmMzSNYikXpxil6BYzJ16611.jpg","genre_ids":[80,9648,53],"original_language":"en","original_title":"The Batman","poster_path":"/74xTEgt7R36Fpooo50r9T25onhq.jpg","id":414906,"video":false,"title":"The Batman","overview":"In his second year of fighting crime, Batman uncovers corruption in Gotham City that connects to his own family while facing a serial killer known as the Riddler.","release_date":"2022-03-01","vote_count":2763,"vote_average":7.9,"popularity":2781.905,"media_type":"movie"}]
/// total_pages : 1000
/// total_results : 20000

class MovieTrendingModel {
  MovieTrendingModel({
    this.page,
    this.results,
    this.totalPages,
    this.totalResults,
  });

  MovieTrendingModel.fromJson(dynamic json) {
    page = json['page'];
    if (json['results'] != null) {
      results = [];
      json['results'].forEach((v) {
        results?.add(Results.fromJson(v));
      });
    }
    totalPages = json['total_pages'];
    totalResults = json['total_results'];
  }

  int? page;
  List<Results>? results;
  int? totalPages;
  int? totalResults;

  MovieTrendingModel copyWith({
    int? page,
    List<Results>? results,
    int? totalPages,
    int? totalResults,
  }) =>
      MovieTrendingModel(
        page: page ?? this.page,
        results: results ?? this.results,
        totalPages: totalPages ?? this.totalPages,
        totalResults: totalResults ?? this.totalResults,
      );

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['page'] = page;
    if (results != null) {
      map['results'] = results?.map((v) => v.toJson()).toList();
    }
    map['total_pages'] = totalPages;
    map['total_results'] = totalResults;
    return map;
  }
}

/// genre_ids : [53,28]
/// original_language : "en"
/// original_title : "All the Old Knives"
/// poster_path : "/g4tMniKxol1TBJrHlAtiDjjlx4Q.jpg"
/// video : false
/// vote_average : 6.2
/// id : 294793
/// release_date : "2022-04-08"
/// vote_count : 62
/// title : "All the Old Knives"
/// adult : false
/// backdrop_path : "/xicKILMzPn6XZYCOpWwaxlUzg6S.jpg"
/// overview : "When the CIA discovers one of its agents leaked information that cost more than 100 people their lives, veteran operative Henry Pelham is assigned to root out the mole with his former lover and colleague Celia Harrison."
/// popularity : 137.5
/// media_type : "movie"

class Results {
  Results({
    this.genreIds,
    this.originalLanguage,
    this.originalTitle,
    this.posterPath,
    this.video,
    this.voteAverage,
    this.id,
    this.releaseDate,
    this.voteCount,
    this.title,
    this.adult,
    this.backdropPath,
    this.overview,
    this.popularity,
    this.mediaType,
  });

  Results.fromJson(dynamic json) {
    genreIds = json['genre_ids'] != null ? json['genre_ids'].cast<int>() : [];
    originalLanguage = json['original_language'];
    originalTitle = json['original_title'];
    posterPath = json['poster_path'];
    video = json['video'];
    voteAverage = json['vote_average'];
    id = json['id'];
    releaseDate = json['release_date'];
    voteCount = json['vote_count'];
    title = json['title'] ?? json['original_name'];
    adult = json['adult'];
    backdropPath = json['backdrop_path'];
    overview = json['overview'];
    popularity = json['popularity'];
    mediaType = json['media_type'];
  }

  List<int>? genreIds;
  String? originalLanguage;
  String? originalTitle;
  String? posterPath;
  bool? video;
  double? voteAverage;
  int? id;
  String? releaseDate;
  int? voteCount;
  String? title;
  bool? adult;
  String? backdropPath;
  String? overview;
  double? popularity;
  String? mediaType;

  Results copyWith({
    List<int>? genreIds,
    String? originalLanguage,
    String? originalTitle,
    String? posterPath,
    bool? video,
    double? voteAverage,
    int? id,
    String? releaseDate,
    int? voteCount,
    String? title,
    bool? adult,
    String? backdropPath,
    String? overview,
    double? popularity,
    String? mediaType,
  }) =>
      Results(
        genreIds: genreIds ?? this.genreIds,
        originalLanguage: originalLanguage ?? this.originalLanguage,
        originalTitle: originalTitle ?? this.originalTitle,
        posterPath: posterPath ?? this.posterPath,
        video: video ?? this.video,
        voteAverage: voteAverage ?? this.voteAverage,
        id: id ?? this.id,
        releaseDate: releaseDate ?? this.releaseDate,
        voteCount: voteCount ?? this.voteCount,
        title: title ?? this.title,
        adult: adult ?? this.adult,
        backdropPath: backdropPath ?? this.backdropPath,
        overview: overview ?? this.overview,
        popularity: popularity ?? this.popularity,
        mediaType: mediaType ?? this.mediaType,
      );

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['genre_ids'] = genreIds;
    map['original_language'] = originalLanguage;
    map['original_title'] = originalTitle;
    map['poster_path'] = posterPath;
    map['video'] = video;
    map['vote_average'] = voteAverage;
    map['id'] = id;
    map['release_date'] = releaseDate;
    map['vote_count'] = voteCount;
    map['title'] = title;
    map['adult'] = adult;
    map['backdrop_path'] = backdropPath;
    map['overview'] = overview;
    map['popularity'] = popularity;
    map['media_type'] = mediaType;
    return map;
  }
}
