import 'package:flutter/material.dart';

Widget loading(double sized, {double diameter = 40}) {
  return Container(
    height: diameter,
    width: diameter,
    child: CircularProgressIndicator.adaptive(),
  );
}
