import 'package:flutter_bloc/flutter_bloc.dart';

class ObsecureTextBlocCubit extends Cubit<bool> {
  ObsecureTextBlocCubit() : super(false);

  void changeObsecureText() => emit(!state);
}