import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:majootestcase/main.dart';
import 'package:majootestcase/models/user/user_model.dart';
import 'package:majootestcase/services/db_local/db_helper.dart';
import 'package:majootestcase/ui/home/home_page.dart';
import 'package:majootestcase/ui/login/login_page.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  void fetchHistoryLogin(BuildContext context) async {
    emit(AuthBlocInitialState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool? isLoggedIn = sharedPreferences.getBool(Preference.IS_LOGGED_IN);
    print(isLoggedIn);
    if (isLoggedIn == null) {
      emit(AuthBlocLoginState());
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => LoginPage()));
    } else {
      if (isLoggedIn) {
        emit(AuthBlocLoggedInState());
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => HomePage()));
      } else {
        emit(AuthBlocLoginState());
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => LoginPage()));
      }
    }
  }

  void loginUser(UserModel user) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    emit(AuthBlocLoadingState());
    print(user.email);
    print(user.password);
    final res =
        await DbHelper.login(email: user.email!, password: user.password!);
    log(res.toString(), name: 'response login');
    if (res == null) {
      emit(AuthBlocErrorState('Email dan Password tidak terdaftar'));
    } else {
      await sharedPreferences.setBool(Preference.IS_LOGGED_IN, true);
      String data = user.toMap().toString();
      sharedPreferences.setString(Preference.USER_VALUE, data);
      emit(AuthBlocLoggedInState());
    }
  }

  void register(UserModel user) async {
    emit(RegisterBlocLoadingState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.setBool(Preference.IS_LOGGED_IN, true);
    String data = user.toMap().toString();
    await sharedPreferences.setString(Preference.USER_VALUE, data);
    await DbHelper.register(
        username: user.userName!, email: user.email!, password: user.password!);
    emit(RegisterBlocSuccesState(user));
  }

  void logout(BuildContext context) async {
    Navigator.pushAndRemoveUntil(context,
        MaterialPageRoute(builder: (context) => LoginPage()), (route) => false);
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.setBool(Preference.IS_LOGGED_IN, false);
    await sharedPreferences.remove(Preference.USER_VALUE);
  }
}
