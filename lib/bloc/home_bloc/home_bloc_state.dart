part of 'home_bloc_cubit.dart';

abstract class HomeBlocState extends Equatable {
  const HomeBlocState();

  @override
  List<Object> get props => [];
}

class HomeBlocInitial extends HomeBlocState {}

class HomeBlocLoading extends HomeBlocState {}

class HomeBlocLoaded extends HomeBlocState {
  final MovieTrendingModel? listMovieTrending;

  HomeBlocLoaded({this.listMovieTrending});

  @override
  // TODO: implement props
  List<Object> get props => [listMovieTrending!];
}

class HomeBlocError extends HomeBlocState {
  final Failure failure;

  HomeBlocError({required this.failure});

  @override
  // TODO: implement props
  List<Object> get props => [failure];
}
