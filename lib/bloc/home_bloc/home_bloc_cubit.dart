import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/home/movie_trending_model.dart';
import 'package:majootestcase/repository/api_repository.dart';

import '../../models/home/failure_model.dart';

part 'home_bloc_state.dart';

class HomeBlocCubit extends Cubit<HomeBlocState> {
  final ApiRepository apiRepository;

  HomeBlocCubit({required this.apiRepository}) : super(HomeBlocInitial());

  Future<void> fetchMovieTrending() async {
    emit(HomeBlocLoading());
    try {
      final MovieTrendingModel? listMovieTrending =
          await apiRepository.getMovieTrending();
      print(listMovieTrending.toString());
      emit(HomeBlocLoaded(listMovieTrending: listMovieTrending));
    } on Failure catch (err) {
      emit(HomeBlocError(failure: err));
    } catch (err) {
      print('Error: $err');
    }
  }
}
