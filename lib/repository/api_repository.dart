import 'dart:developer';

import 'package:majootestcase/models/home/movie_trending_model.dart';
import 'package:majootestcase/services/api_service.dart';

class ApiRepository {
  ApiRepository(this.apiServices);

  final ApiServices apiServices;

  Future<MovieTrendingModel?> getMovieTrending() async {
    final response = await apiServices.getMovieData();
    print(response?.data['results']);
    if (response!.data['results'] != null) {
      return MovieTrendingModel.fromJson(response.data);
    }
    return null;
  }
}
